import React, { Component } from 'react';
import Web3 from 'web3'
import './App.css';
import Identity from '../abis/Identity.json'

class App extends Component {

  async componentWillMount() {
    await this.loadWeb3()
    await this.loadBlockchainData()
  }

  async loadWeb3() {
    if (window.ethereum) {
      window.web3 = new Web3(window.ethereum)
      await window.ethereum.enable()
    }
    else if (window.web3) {
      window.web3 = new Web3(window.web3.currentProvider)
    }
    else {
      window.alert('Non-Ethereum browser detected. You should consider trying MetaMask!')
    }
  }

  async loadBlockchainData() {
    const web3 = window.web3
    // Load account
    const accounts = await web3.eth.getAccounts()
    this.setState({ account: accounts[0] })

    const networkId = await web3.eth.net.getId()
    const networkData = Identity.networks[networkId]
    if(networkData) {
      const abi = Identity.abi
      const address = networkData.address
      const contract = new web3.eth.Contract(abi, address)
      this.setState({ contract })
      const totalSupply = await contract.methods.totalSupply().call()
      this.setState({ totalSupply })
      // Load Identities
      for (var i = 1; i <= totalSupply; i++) {
        const indentity = await contract.methods.identities(i - 1).call()
        this.setState({
          identities: [...this.state.identities, indentity]
        })
      }
    } else {
      window.alert('Smart contract not deployed to detected network.')
    }
  }

  mint = (name) => {
    this.state.contract.methods.mint(name).send({ from: this.state.account })
    .once('receipt', (receipt) => {
      this.setState({
        identities: [...this.state.identities, name]
      })
    })
  }

  constructor(props) {
    super(props)
    this.state = {
      account: '',
      contract: null,
      totalSupply: 0,
      identities: []
    }
  }

  render() {
    return (
      <div>
        <nav className="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
          
        <small className="text-white">Indetity Tokens</small>
          
          <ul className="navbar-nav px-3">
            <li className="nav-item text-nowrap d-none d-sm-none d-sm-block">
              <small className="text-white"><span id="account">{this.state.account}</span></small>
            </li>
          </ul>
        </nav>
        <div className="container-fluid mt-5">
          <div className="row">
            <main role="main" className="col-lg-12 d-flex text-center">
              <div className="content mr-auto ml-auto">
                <h1>Name Token</h1>
                <form onSubmit={(event) => {
                  event.preventDefault()
                  const name = this.name.value
                  this.mint(name)
                }}>
                  <input
                    type='text'
                    className='form-control mb-1'
                    placeholder='e.g. Pedro'
                    ref={(input) => { this.name = input }}
                  />
                  <input
                    type='submit'
                    className='btn btn-block btn-primary'
                    value='MINT'
                  />
                </form>
              </div>
            </main>
          </div>
          <hr/>
          <div className="row text-center">
            { this.state.identities.map((name, key) => {
              return(
                <div key={key} className="col-md-3 mb-3">
                  <div className="token" ></div>
                  <div>{name}</div>
                </div>
              )
            })}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
