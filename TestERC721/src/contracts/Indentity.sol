pragma solidity 0.5.0;

import "./ERC721Full.sol";

contract Identity is ERC721Full {
  string[] public identities;
  mapping(string => bool) _nameExists;

  constructor() ERC721Full("Identity", "IDY") public {
  }

  
  function mint(string memory _name) public {
    require(!_nameExists[_name]);
    uint _id = identities.push(_name);
    _mint(msg.sender, _id);
    _nameExists[_name] = true;
  }

}
