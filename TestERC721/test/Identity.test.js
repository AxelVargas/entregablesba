const Identity = artifacts.require('./Identity.sol')

require('chai')
  .use(require('chai-as-promised'))
  .should()

contract('Identity', (accounts) => {
  let contract

  before(async () => {
    contract = await Identity.deployed()
  })

  describe('deployment', async () => {
    it('deploys successfully', async () => {
      const address = contract.address
      assert.notEqual(address, 0x0)
      assert.notEqual(address, '')
      assert.notEqual(address, null)
      assert.notEqual(address, undefined)
    })

    it('has a name', async () => {
      const name = await contract.name()
      assert.equal(name, 'Identity')
    })

    it('has a symbol', async () => {
      const symbol = await contract.symbol()
      assert.equal(symbol, 'IDY')
    })

  })

  describe('minting', async () => {

    it('creates a new token', async () => {
      const result = await contract.mint('Axel Vargas')
      const totalSupply = await contract.totalSupply()
      // SUCCESS
      assert.equal(totalSupply, 1)
      const event = result.logs[0].args
      assert.equal(event.tokenId.toNumber(), 1, 'id is correct')
      assert.equal(event.from, '0x0000000000000000000000000000000000000000', 'from is correct')
      assert.equal(event.to, accounts[0], 'to is correct')

      // FAILURE: cannot mint same Identity twice
      await contract.mint('Axel Vargas').should.be.rejected;
    })
  })

  describe('indexing', async () => {
    it('lists Identitys', async () => {
      // Mint 3 more tokens
      await contract.mint('Pedro Paramo')
      await contract.mint('Jorge Luis')
      await contract.mint('Alejandro Magno')
      const totalSupply = await contract.totalSupply()

      let Identity
      let result = []

      for (var i = 1; i <= totalSupply; i++) {
        Identity = await contract.identities(i - 1)
        result.push(Identity)
      }

      let expected = ['Axel Vargas', 'Pedro Paramo', 'Jorge Luis', 'Alejandro Magno']
      assert.equal(result.join(','), expected.join(','))
    })
  })

})
