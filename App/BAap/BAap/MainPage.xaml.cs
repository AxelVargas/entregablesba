﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using Nethereum.Web3;
using Nethereum.Signer;
using Nethereum.Hex.HexConvertors.Extensions;
using System.Net;
using Xamarin.Essentials;

using System.IO;

namespace BAap
{
    public partial class MainPage : ContentPage
    {
        public string responseUsr;
        public MainPage()
        {
            InitializeComponent();
        }

        public void OnButtonClicked(object sender, EventArgs e)
        {
            var ecKey = Nethereum.Signer.EthECKey.GenerateKey();
            var privateKey = ecKey.GetPrivateKeyAsBytes().ToHex();
            var account = new Nethereum.Web3.Accounts.Account(privateKey);
            Console.WriteLine(account.Address);
            Console.WriteLine(privateKey);
            Preferences.Set("privateKey", privateKey);
            Preferences.Set("address", account.Address);
            var signer = new EthereumMessageSigner();
            var nonce = password.Text; ;
            var signature = signer.EncodeUTF8AndSign(nonce, new EthECKey(privateKey));
            Preferences.Set("signature", signature);
            Console.WriteLine(signature);

            PostItem(signature, nonce, account.Address, user.Text,name.Text);
            
            (sender as Button).Text = "Done";

        }

        public void PostItem(string signature, string nonce, string address,string user, string name)
        {
            var url = $"http://10.0.2.2:3000/api/new/users";
            var request = (HttpWebRequest)WebRequest.Create(url);
            string json = $"{{\"nonce\":\"{nonce}\", \"signature\":\"{signature}\", \"address\":\"{address}\",\"idDatabase\":\"2\",\"userName\":\"{user}\",\"name\":\"{name}\"}}";
            Console.WriteLine(json);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Accept = "application/json";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream strReader = response.GetResponseStream())
                    {
                        if (strReader == null) return ;
                        using (StreamReader objReader = new StreamReader(strReader))
                        {
                            string responseBody = objReader.ReadToEnd();

                            Console.WriteLine(responseBody);
                            
                            
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void OnButtonClickedUsr(object sender, EventArgs e)
        {


            var address = Preferences.Get("address", "default_value");
            var signature = Preferences.Get("signature", "default_value");            
            var nonce = this.nonce.Text; ;

            
            

            var url = $"http://10.0.2.2:3000/api/get/users";
            var request = (HttpWebRequest)WebRequest.Create(url);
            string json = $"{{\"nonce\":\"{nonce}\", \"signature\":\"{signature}\", \"address\":\"{address}\"}}";

            request.Method = "POST";
            request.ContentType = "application/json";
            request.Accept = "application/json";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            var th = this;
            try
            {

                using (WebResponse response = request.GetResponse())
                {
                    using (Stream strReader = response.GetResponseStream())
                    {
                        if (strReader == null) return;
                        using (StreamReader objReader = new StreamReader(strReader))
                        {
                            responseUsr = objReader.ReadToEnd();

                            Console.WriteLine(responseUsr);



                        }
                    }
                }
                respo.Text = responseUsr;
            }
            catch (WebException ex)
            {
                Console.WriteLine(ex);
            }



            (sender as Button).Text = "Read";

        }
    }
}
