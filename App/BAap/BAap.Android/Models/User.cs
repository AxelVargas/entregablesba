﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace BAap.Droid.Models
{
    public class User
    {
        public string userName { get; set; }
        public string metadata { get; set; }
    }
}