pragma solidity 0.5.16;


contract Auth {
    address owner;
    constructor() public {
        owner = msg.sender;
    }
    modifier onlyOwner {
       require(msg.sender == owner);
       _;
   }

}

contract UserIdentity is Auth {
   
    struct User {
        string userName;
        string idDataBase;
        string metadata;
    }

    struct UserInfo {
        string curp;
        string rfc;
        string metadata;
    }

    struct UserFile {
        string typeFile;
        bytes32 file;
    }

    mapping (address => User) private users; 
    mapping (address => address[]) private usersHistoryIndex;
    mapping (address => User[]) private usersHistory;
    
    mapping (address => UserInfo) private usersInfo; 
    mapping (address => address[]) private usersInfoHistoryIndex;
    mapping (address => UserInfo[]) private usersInfoHistory;

    mapping (address => UserFile) private usersFile; 
    mapping (address => address[]) private usersFileHistoryIndex;
    mapping (address => UserFile[]) private usersFileHistory;
    
    function upsertUser(address _address, string memory _userName,string memory _idDataBase, string memory _metadata,bytes32 _nonce,bytes memory _signature ) public payable onlyOwner {
        address signer = recoverSigner(_nonce,_signature);
        require(signer == _address);
        User memory user = users[_address];
        user.userName= _userName;
        user.idDataBase= _idDataBase;
        user.metadata= _metadata;
        usersHistory[_address].push(user);
        users[_address] = user;
        usersHistoryIndex[_address].push(_address) -1;

    }


    function getUsersHistoryIndex(address _address,bytes32  _nonce,bytes memory _signature) public view returns(uint256) {
        address signer = recoverSigner(_nonce,_signature);
        require(signer == _address);
        return usersHistoryIndex[_address].length;
    }

    function getUsersHistoryByIndex(address _address, uint160 index,bytes32  _nonce,bytes memory _signature) public view returns(string memory, string memory, string memory) {
        address signer = recoverSigner(_nonce,_signature);
        require(signer == _address);
        return (usersHistory[_address][index].userName,
                usersHistory[_address][index].idDataBase,
                usersHistory[_address][index].metadata);
    }

    function getUser(address _address,bytes32 _nonce,bytes memory _signature) public view returns (string memory,string memory, string memory) {
        address signer = recoverSigner(_nonce,_signature);
        require(signer == _address);
        return (users[_address].userName,
                users[_address].idDataBase,
                users[_address].metadata);
    }


    function upsertUserInfo(address _address, string memory _curp,string memory _rfc, string memory _metadata,bytes32 _nonce,bytes memory _signature ) public payable onlyOwner {
        address signer = recoverSigner(_nonce,_signature);
        require(signer == _address);
        UserInfo memory userInfo = usersInfo[_address];
        userInfo.curp= _curp;
        userInfo.rfc= _rfc;
        userInfo.metadata= _metadata;
        usersInfoHistory[_address].push(userInfo);
        usersInfo[_address] = userInfo;
        usersInfoHistoryIndex[_address].push(_address) -1;

    }


    function getUsersInfoHistoryIndex(address _address,bytes32  _nonce,bytes memory _signature) public view returns(uint256) {
        address signer = recoverSigner(_nonce,_signature);
        require(signer == _address);
        return usersInfoHistoryIndex[_address].length;
    }

    function getUsersInfoHistoryByIndex(address _address, uint160 index,bytes32  _nonce,bytes memory _signature) public view returns(string memory, string memory, string memory) {
        address signer = recoverSigner(_nonce,_signature);
        require(signer == _address);
        return (usersInfoHistory[_address][index].curp,
                usersInfoHistory[_address][index].rfc,
                usersInfoHistory[_address][index].metadata);
    }

    function getUserInfo(address _address,bytes32 _nonce,bytes memory _signature) public view returns (string memory, string memory, string memory) {
        address signer = recoverSigner(_nonce,_signature);
        require(signer == _address);
        return (usersInfo[_address].curp,
                usersInfo[_address].rfc,
                usersInfo[_address].metadata);
    }


    function upsertUserFile(address _address, string memory _typeFile,bytes32 _file,bytes32 _nonce,bytes memory _signature ) public payable onlyOwner {
        address signer = recoverSigner(_nonce,_signature);
        require(signer == _address);
        UserFile memory userFile = usersFile[_address];
        userFile.typeFile= _typeFile;
        userFile.file= _file;
        usersFileHistory[_address].push(userFile);
        usersFile[_address] = userFile;
        usersFileHistoryIndex[_address].push(_address) -1;

    }


    function getUsersFileHistoryIndex(address _address,bytes32  _nonce,bytes memory _signature) public view returns(uint256) {
        address signer = recoverSigner(_nonce,_signature);
        require(signer == _address);
        return usersFileHistoryIndex[_address].length;
    }

    function getUsersFileHistoryByIndex(address _address, uint160 index,bytes32  _nonce,bytes memory _signature) public view returns(string memory, bytes32) {
        address signer = recoverSigner(_nonce,_signature);
        require(signer == _address);
        return (usersFileHistory[_address][index].typeFile,
                usersFileHistory[_address][index].file);
    }

    function getUserFile(address _address,bytes32 _nonce,bytes memory _signature) public view returns (string memory,bytes32) {
        address signer = recoverSigner(_nonce,_signature);
        require(signer == _address);
        return (usersFile[_address].typeFile,
                usersFile[_address].file);
    }



      function recoverSigner(bytes32 nonce, bytes memory signature)
       public
       pure
       returns (address)
    {
       uint8 v;
       bytes32 r;
       bytes32 s;

       (v, r, s) = splitSignature(signature);
       return ecrecover(nonce, v, r, s);
  }

  function splitSignature(bytes memory signature)
       public
       pure
       returns (uint8, bytes32, bytes32)
   {
       require(signature.length == 65);
       
       bytes32 r;
       bytes32 s;
       uint8 v;

       assembly {
           r := mload(add(signature, 32))
           s := mload(add(signature, 64))
           v := byte(0, mload(add(signature, 96)))
       }

       return (v, r, s);
   }


}