const { default: Web3 } = require("web3");

const BetsBlockchain = artifacts.require('BetsBlockchain');

let instance;

beforeEach(async () => {
    instance = await BetsBlockchain.new();
});

contract('BetsBlockchain', accounts => {

    it('should create office', async () => {

        let client = accounts[1];
        await instance.setOffice(client,"TEST NAME","sss","DDD","DDD", "AAA")
        let officeData = await instance.getOffice(client);
        assert.equal("TEST NAME", officeData['0'], 'Invalid Office');
    });

});