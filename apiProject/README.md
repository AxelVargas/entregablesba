# String to BYTES32


# Configuration if execution failed
rm -R -f build #cleanup step 
rm -R -f node_modules #cleanup step 
npm uninstall -g truffle
npm install -g truffle
npm install

# deploy local
truffle migrate --network development --reset

# configuration deploy production
Instalar truffle-hdwallet-provider
    npm i truffle-hdwallet-provider

Agregar a truffle-config.js
    const HDWalletProvider = require("truffle-hdwallet-provider");

En la red de producción agregar el metedo
    provider: () => {
        return new HDWalletProvider(MNEMONIC, RPC_URL)
      },
La red se vería así
    nameNetwork: {
      provider: () => {
        return new HDWalletProvider(envConfig.MNEMONIC, envConfig.RPC_URL)
      },
      network_id: 3,
      skipDryRun: true,
      production: false,
      gasPrice: 7000000000,
    }

Donde MNEMONIC es la semilla de la wallet con los fondos para el deployment y RPC_URL es el Endpoint que enviara el abi.
El endpoint se puede obtener al crear un proyecto en https://infura.io/ 

Posterior a eso se ejecutan los siguientes comandos
    truffle compile
    truffle migrate --network nameNetwork --reset
Esto realizará la migración del contrato a producción.

# console
truffle console --network development

# show

https://blockchangers.github.io/solidity-converter-online/

# tests on console

//Office 01
let contract = await BetsBlockchain.deployed()
contract.upsertOffice("0x5Af7f5B8B345B136075FE6e0Bb21bCeEF87c9ca8","Betpredator","metadata")
contract.upsertOffice("0x5Af7f5B8B345B136075FE6e0Bb21bCeEF87c9ca8","Betpredator","metadata2")

contract.getOfficesIndex()
contract.getOfficesHistoryIndex("0x5Af7f5B8B345B136075FE6e0Bb21bCeEF87c9ca8")
contract.getOfficesHistoryByIndex("0x5Af7f5B8B345B136075FE6e0Bb21bCeEF87c9ca8", 0)
contract.getOfficesHistoryByIndex("0x5Af7f5B8B345B136075FE6e0Bb21bCeEF87c9ca8", 1)

//Office 02
contract.upsertOffice("0xeE8602ec925ee478754b337A8c9256a12A3ee972","Itz","metadata2") 

// Client 1
let contract = await BetsBlockchain.deployed()
contract.upsertclient("0x5Af7f5B8B345B136075FE6e0Bb21bCeEF87c9ca8","0xC1c7Dfecc40303169204a1e2856dEFF0e70b6399","metadata")
contract.upsertclient("0x5Af7f5B8B345B136075FE6e0Bb21bCeEF87c9ca8","0xC1c7Dfecc40303169204a1e2856dEFF0e70b6399","metadata2")

// Client 2
let contract = await BetsBlockchain.deployed()
contract.upsertclient("0x5Af7f5B8B345B136075FE6e0Bb21bCeEF87c9ca8","0x67862A92DEc1CE4569070B85Ba18187EBEca7882","metadata");
contract.upsertclient("0x5Af7f5B8B345B136075FE6e0Bb21bCeEF87c9ca8","0x67862A92DEc1CE4569070B85Ba18187EBEca7882","metadata2");
contract.getClientsIndex();
contract.getClient("0x67862A92DEc1CE4569070B85Ba18187EBEca7882");
contract.getClientsHistoryIndex('0xC1c7Dfecc40303169204a1e2856dEFF0e70b6399')
contract.getClientsHistoryByIndex('0xC1c7Dfecc40303169204a1e2856dEFF0e70b6399',0)
contract.getClientsHistoryByIndex('0xC1c7Dfecc40303169204a1e2856dEFF0e70b6399',1)
contract.getClientsByOffice('0x5Af7f5B8B345B136075FE6e0Bb21bCeEF87c9ca8')

//Ticket 01 Client 1
contract.upsertTicket("0xaeea17de3bfd0dd339a2938a72aaf3a13641bd50","0x5Af7f5B8B345B136075FE6e0Bb21bCeEF87c9ca8","0xC1c7Dfecc40303169204a1e2856dEFF0e70b6399","1","Caballos","metadata")
contract.upsertTicket("comprimido03","0x5Af7f5B8B345B136075FE6e0Bb21bCeEF87c9ca8","0xC1c7Dfecc40303169204a1e2856dEFF0e70b6399","3","Caballos","metadata3")

//Ticket 02
contract.upsertTicket("comprimido02","0x5Af7f5B8B345B136075FE6e0Bb21bCeEF87c9ca8","0x67862A92DEc1CE4569070B85Ba18187EBEca7882","2","Casino","metadata2")
contract.upsertTicket("comprimido02","0x5Af7f5B8B345B136075FE6e0Bb21bCeEF87c9ca8","0x67862A92DEc1CE4569070B85Ba18187EBEca7882","2","Casino","metadata2up")
contract.upsertTicket("comprimido04","0xeE8602ec925ee478754b337A8c9256a12A3ee972","0x67862A92DEc1CE4569070B85Ba18187EBEca7882","4","Futbol","metadata4")
contract.upsertTicket("comprimido04","0xeE8602ec925ee478754b337A8c9256a12A3ee972","0x67862A92DEc1CE4569070B85Ba18187EBEca7882","4","Futbol","metadata4up")

//Busquedq por cliente
contract.getTicketsIndexByClient("0xC1c7Dfecc40303169204a1e2856dEFF0e70b6399")
contract.getTicket("comprimido01")
contract.getTicketsHistoryIndex("0xC1c7Dfecc40303169204a1e2856dEFF0e70b6399")
contract.getTicketsHistoryByIndex("0xC1c7Dfecc40303169204a1e2856dEFF0e70b6399",0)
contract.getTicketsHistoryByIndex("0xC1c7Dfecc40303169204a1e2856dEFF0e70b6399",1)
contract.getTicketsHistoryByIndex("0xC1c7Dfecc40303169204a1e2856dEFF0e70b6399",2)
contract.getTicketsIndexByOffice("0xFBBdafc1a449351B8Ac3371A4D7E09EF89893905")

0x9876c6aaD2EedE736311d99B63858D87A96213AB

# Questions
1.Podemos saber todos los ticket de todas las oficinas - Numero de tickets 
2.Tickets de una oficina en particular - Numero de ticket por oficina
3.Traer historial del ticket por oficina - recibe md5 (Arreglo de todo el cambio)
4.Tickets de un cliente en una oficina - recibir cliente y oficina - Indices

# DB

services_dev

blockchain
    offices
        id
        account_id
        private_key
        officeName
        metadata
        transaction_id
    clients
        id
        office_account_id
        account_id
        private_id
        metadata
        transaction_id
