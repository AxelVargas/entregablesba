require('dotenv').config()
const envConfig = process.env
const fs = require('fs')
const Web3 = require("web3");
const web3 = new Web3(new Web3.providers.HttpProvider(envConfig.RPC_URL));
const EthereumTx = require('ethereumjs-tx').Transaction
const jsonFile = "./build/contracts/UserIdentity.json";
const parsed = JSON.parse(fs.readFileSync(jsonFile));
const abi = parsed.abi;
const credentialChainAddress = envConfig.ADDRESS;
const credentialChainPrivateKey =  envConfig.PRIVATE_KEY;
const credentialChainContractAddress = envConfig.CONTRACT;
const privateKey = Buffer.from(
            credentialChainPrivateKey,
            'hex',
        )

class Web3Connector {
    
   static async create(_data,_gasLimit,_gasPrice){
       try{
        let txCount = await web3.eth.getTransactionCount(credentialChainAddress, "pending");
        const txObject = {
            nonce: web3.utils.toHex(txCount),
            gasLimit: web3.utils.toHex(_gasLimit),
            gasPrice: web3.utils.toHex(_gasPrice),
            to: credentialChainContractAddress,
            data: _data
        }
        const tx = new EthereumTx(txObject,{ chain: envConfig.CHAIN, hardfork: envConfig.HARDFORK})
        tx.sign(privateKey)
        const serializedTx = tx.serialize()
        const raw = "0x" + serializedTx.toString("hex");
        let txHash = await web3.eth.sendSignedTransaction(raw);
        return txHash
        
       }catch (error){
           console.log("error:::", error)
       }
    
   }

   static async userExist(userAddress,nonce,signature){
    if(!Web3.utils.isAddress(userAddress)){
        return  {validate:false , info: "client_id is not an valid address", status: 400}
    }
    let clientResult = await new web3.eth.Contract(abi, credentialChainContractAddress)
    .methods.getUser(userAddress,nonce,signature)
    .call({ from: credentialChainAddress })
    if(clientResult[1].length == 0){
        return  {validate:false , info: "Client does not exist", status: 406}
    }
    return {validate:true , info:clientResult}
}

}
module.exports = Web3Connector;