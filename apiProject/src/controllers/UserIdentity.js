const fs = require('fs')
require('dotenv').config()
const envConfig = process.env
const Web3 = require("web3");
const web3 = new Web3(new Web3.providers.HttpProvider(envConfig.RPC_URL));
const url = require('url');
const crypto = require('crypto')
const jwt = require("jsonwebtoken");
const jsonFile = "./build/contracts/UserIdentity.json";
const parsed = JSON.parse(fs.readFileSync(jsonFile));
const abi = parsed.abi;
const credentialChainAddress = envConfig.ADDRESS;
const credentialChainContractAddress = envConfig.CONTRACT;
const Web3Connector = require('../libraries/Web3Connector')
const moment = require('moment');
const SECRET_KEY = process.env.SECRET_KEY;
const { response } = require('express');


class UserIdentity{
    
    
    static async saveUser(req,res,next){

        let result = {};

        try {
            let {body} = req;
            let account;
            let infoObject = req.headers;
            let logger = fs.createWriteStream('transaction.log', {flags: 'a'})
            let text = " User Transaction ";
            let existsRecord = false;
            let signature;
            let sign;
            let nonce;
            
            if ('user_address' in infoObject && 'signature' in infoObject && 'nonce' in infoObject) {
                account = {address: infoObject.user_address }
                nonce = web3.eth.accounts.hashMessage(infoObject.nonce)
                signature = infoObject.signature
                text += "Update "
                existsRecord = true;
            }
            else{
                account = await web3.eth.accounts.create();
                sign = web3.eth.accounts.sign(infoObject.nonce, account.privateKey);
                nonce = sign.messageHash
                signature = sign.signature
            }
            
            
            if(existsRecord){
                let userResult = await new web3.eth.Contract(abi, credentialChainContractAddress)
                .methods.getUser(account['address'],nonce,signature)
                .call({ from: credentialChainAddress })
                
                if(userResult[0].length == 0){
                    return  res.status(406).send({error: "User does not exist"})
                }
            }
            

            let metadata = {
                name: body.name
            }
            
            let user = {
                userName: body.userName,
                idDatabase: body.idDatabase,
                metadata: JSON.stringify(metadata)
            }
            
            let data = await new web3.eth.Contract(abi, credentialChainContractAddress)
            .methods.upsertUser(account['address'],user.userName,user.idDatabase, user.metadata,nonce,signature).encodeABI()
            let gasLimit = await new web3.eth.Contract(abi, credentialChainContractAddress)
            .methods.upsertUser(account['address'],user.userName,user.idDatabase, user.metadata,nonce,signature).estimateGas({from: credentialChainAddress})
            let gasPrice = await web3.eth.getGasPrice() //Wei
            let txHash = await Web3Connector.create(data,gasLimit,gasPrice) 
            let ehtTotal =gasPrice*gasLimit
            let etherValue = Web3.utils.fromWei(ehtTotal.toString(), 'ether');
            logger.write("\n"+ moment().format()  + text + '- GasLimit: ' + gasLimit.toString() + ' GasPrice: ' + gasPrice.toString() + ' ETH: ' + etherValue.toString())
            logger.end()
            result = {
                user_address: account,
                transaction: txHash,
                signature: signature
            };
            
        } catch (error) {
            console.log("error:::", error)
            return res.status(409).send({error: "Error occurred during execution"})
        }

        return res.json({
            response: result
        });
            
    }

    static async saveUserCs(req,res,next){

        let result = {};

        try {
            let {body} = req;
            let logger = fs.createWriteStream('transaction.log', {flags: 'a'})
            let text = " User Transaction ";
            
            let signature =body.signature;
            let nonce =  web3.eth.accounts.hashMessage(body.nonce)

            let metadata = {
                name: body.name
            }
            
            let user = {
                userName: body.userName,
                idDatabase: body.idDatabase,
                metadata: JSON.stringify(metadata)
            }
            console.log(user)
            let data = await new web3.eth.Contract(abi, credentialChainContractAddress)
            .methods.upsertUser(body.address,user.userName,user.idDatabase, user.metadata,nonce,signature).encodeABI()
            let gasLimit = await new web3.eth.Contract(abi, credentialChainContractAddress)
            .methods.upsertUser(body.address,user.userName,user.idDatabase, user.metadata,nonce,signature).estimateGas({from: credentialChainAddress})
            let gasPrice = await web3.eth.getGasPrice() //Wei
            let txHash = await Web3Connector.create(data,gasLimit,gasPrice) 
            let ehtTotal =gasPrice*gasLimit
            let etherValue = Web3.utils.fromWei(ehtTotal.toString(), 'ether');
            logger.write("\n"+ moment().format()  + text + '- GasLimit: ' + gasLimit.toString() + ' GasPrice: ' + gasPrice.toString() + ' ETH: ' + etherValue.toString())
            logger.end()
            result = {
                user_address: body.address,
                transaction: txHash
            };
            
        } catch (error) {
            console.log("error:::", error)
            return res.status(409).send({error: "Error occurred during execution"})
        }

        return res.json({
            response: result
        });
            
    }

    static async getUserCs(req,res,next){

        let result = {};
        try {
            let {body} = req;
            let nonce =  web3.eth.accounts.hashMessage(body.nonce)
            let user = await new web3.eth.Contract(abi, credentialChainContractAddress)
            .methods.getUser(body.address,nonce,body.signature)
            .call({ from: credentialChainAddress })
            if(user[0].length == 0){
                return  res.status(406).send({error: "User does not exist"})
            }
            result={
                userName: user[0], 
                idDatabase: user[1], 
                metadata: JSON.parse(user[2])
            }
            
        } catch (error) {
            console.log("error:::", error)
            return res.status(409).send({error: "Error occurred during execution"})
        }
        return res.json({
            response: result
        });
            
    }

    static async getUser(req,res,next){
        let userAddress  = req.params.user_address.toString();
        let signature = req.headers.signature
        let nonce = web3.eth.accounts.hashMessage(req.headers.nonce)
        let result = {};
        try {
            
            let user = await new web3.eth.Contract(abi, credentialChainContractAddress)
            .methods.getUser(userAddress,nonce,signature)
            .call({ from: credentialChainAddress })
            
            result={
                userName: user[0], 
                idDatabase: user[1], 
                metadata: JSON.parse(user[2])
            }
            
        } catch (error) {
            console.log("error:::", error)
            return res.status(409).send({error: "Error occurred during execution"})
        }
        return res.json({
            response: result
        });
            
    }


    static async getUserHistory(req,res,next){
        
        let userAddress  = req.params.user_address.toString();
        let result = [];
        let signature = req.headers.signature
        let nonce = web3.eth.accounts.hashMessage(req.headers.nonce)
        try {
            
            let userValidate = await Web3Connector.userExist(userAddress,nonce,signature)
            if(!userValidate.validate){
                return  res.status(userValidate.status).send({error: userValidate.info})
            }
            
            let userLength = await new web3.eth.Contract(abi, credentialChainContractAddress)
            .methods.getUsersHistoryIndex(userAddress,nonce,signature)
            .call({ from: credentialChainAddress })
            
            for(let i=0; i<userLength;i++){
                
                let userResult = await new web3.eth.Contract(abi, credentialChainContractAddress)
                .methods.getUsersHistoryByIndex(userAddress,i,nonce,signature)
                .call({ from: credentialChainAddress })
                
                        let user = {
                            userName: userResult[0], 
                            idDatabase: userResult[1], 
                            metadata: JSON.parse(userResult[2])
                        }
                        

                        result.push(user);
            }
            
        } catch (error) {
            console.log("error:::", error)
        }

        return res.json({
            user_address: userAddress,
            user: result
        });
            
    }

    static async saveUserInfo(req,res,next){

        let result = {};

        try {
            let {body} = req;
            let logger = fs.createWriteStream('transaction.log', {flags: 'a'})
            let text = " UserInfo Transaction ";
            let userAddress  = req.params.user_address.toString();
            let signature = req.headers.signature
            let nonce = web3.eth.accounts.hashMessage(req.headers.nonce)
            
            let userResult = await new web3.eth.Contract(abi, credentialChainContractAddress)
                .methods.getUser(userAddress,nonce,signature)
                .call({ from: credentialChainAddress })
                
            if(userResult[0].length == 0){
                return  res.status(406).send({error: "User does not exist"})
            }

            let metadata = {
                
                infoExtra: body.extra
            }
            
            let user = {
                curp: body.curp,
                rfc: body.rfc,
                metadata: jwt.sign(JSON.stringify(metadata),SECRET_KEY)
            }
            console.log("data: ",user)
            let data = await new web3.eth.Contract(abi, credentialChainContractAddress)
            .methods.upsertUserInfo(userAddress,user.curp, user.rfc,user.metadata,nonce,signature).encodeABI()
            let gasLimit = await new web3.eth.Contract(abi, credentialChainContractAddress)
            .methods.upsertUserInfo(userAddress,user.curp, user.rfc, user.metadata,nonce,signature).estimateGas({from: credentialChainAddress})
            let gasPrice = await web3.eth.getGasPrice() //Wei
            let txHash = await Web3Connector.create(data,gasLimit,gasPrice) 
            let ehtTotal =gasPrice*gasLimit
            let etherValue = Web3.utils.fromWei(ehtTotal.toString(), 'ether');
            logger.write("\n"+ moment().format()  + text + '- GasLimit: ' + gasLimit.toString() + ' GasPrice: ' + gasPrice.toString() + ' ETH: ' + etherValue.toString())
            logger.end()
            result = {
                user_address: userAddress,
                transaction: txHash
            };
            
        } catch (error) {
            console.log("error:::", error)
            return res.status(409).send({error: "Error occurred during execution"})
        }

        return res.json({
            response: result
        });
            
    }

    static async getUserInfoEnc(req,res,next){
        let userAddress  = req.params.user_address.toString();
        let signature = req.headers.signature
        let nonce = web3.eth.accounts.hashMessage(req.headers.nonce)
        let result = {};
        try {
            
            let userResult = await new web3.eth.Contract(abi, credentialChainContractAddress)
            .methods.getUser(userAddress,nonce,signature)
            .call({ from: credentialChainAddress })
            
        if(userResult[0].length == 0){
            return  res.status(406).send({error: "User does not exist"})
        }
        let userInfoResult = await new web3.eth.Contract(abi, credentialChainContractAddress)
        .methods.getUserInfo(userAddress,nonce,signature)
        .call({ from: credentialChainAddress })

        if(userInfoResult[0].length == 0){
            return  res.status(406).send({error: "UserInfo does not exist"})
        }
            result={
                curp: userInfoResult[0], 
                rfc: userInfoResult[1], 
                metadata: userInfoResult[2]
            }
            
        } catch (error) {
            console.log("error:::", error)
            return res.status(409).send({error: "Error occurred during execution"})
        }
        return res.json({
            response: result
        });
            
    }

    static async getUserInfo(req,res,next){
        let userAddress  = req.params.user_address.toString();
        let signature = req.headers.signature
        let nonce = web3.eth.accounts.hashMessage(req.headers.nonce)
        let result = {};
        try {
            
            let userResult = await new web3.eth.Contract(abi, credentialChainContractAddress)
            .methods.getUser(userAddress,nonce,signature)
            .call({ from: credentialChainAddress })
            
        if(userResult[0].length == 0){
            return  res.status(406).send({error: "User does not exist"})
        }
        let userInfoResult = await new web3.eth.Contract(abi, credentialChainContractAddress)
        .methods.getUserInfo(userAddress,nonce,signature)
        .call({ from: credentialChainAddress })

        if(userInfoResult[0].length == 0){
            return  res.status(406).send({error: "UserInfo does not exist"})
        }
            result={
                curp: userInfoResult[0], 
                rfc: userInfoResult[1], 
                metadata: jwt.verify(userInfoResult[2], SECRET_KEY)
            }
            
        } catch (error) {
            console.log("error:::", error)
            return res.status(409).send({error: "Error occurred during execution"})
        }
        return res.json({
            response: result
        });
            
    }

}
module.exports = UserIdentity;