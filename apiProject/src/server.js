const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const UserIdentity = require('./controllers/UserIdentity')
const PORT = process.env.PORT || 3000;

app.use(express.json()) 
app.use(bodyParser.urlencoded({ extended: false }));


app.get('/status', function (req, res) {
  res.json({
      response: "OK"
  });
});

//Offices
app.post('/api/users', UserIdentity.saveUser)
app.post('/api/new/users', UserIdentity.saveUserCs)
app.post('/api/get/users/', UserIdentity.getUserCs) 
app.get('/api/users/:user_address', UserIdentity.getUser) 
app.get('/api/users/history/:user_address', UserIdentity.getUserHistory)
app.post('/api/users/info/:user_address', UserIdentity.saveUserInfo) 
app.get('/api/users/enc/:user_address', UserIdentity.getUserInfoEnc) 
app.get('/api/users/info/:user_address', UserIdentity.getUserInfo) 
app.listen(PORT, function () {
  console.log(`App listening on port http://localhost:${PORT}`);
});