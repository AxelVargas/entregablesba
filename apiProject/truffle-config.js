const HDWalletProvider = require("truffle-hdwallet-provider");
require('dotenv').config()
const envConfig = process.env

module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*",
      skipDryRun: true,
      production: false
      // gas: 1163105,
      // gasPrice: 2000000000
    },
    compilers: {
      solc: {
        version: "0.5.16" // ex:  "0.4.20". (Default: Truffle's installed solc)
      }
   },
    ropsten: {
      provider: () => {
        return new HDWalletProvider(envConfig.MNEMONIC, envConfig.RPC_URL)
      },
      network_id: 3,
      //gas: 3307712,     //make sure this gas allocation isn't over 4M, which is the max
      skipDryRun: true,
      production: false,
      //gasPrice: 5000000000,
    }
  },
  plugins: ["solidity-coverage"]
};